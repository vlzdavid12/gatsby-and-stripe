## Gatsby And Strype
![Alt Text](https://gitlab.com/vlzdavid12/gatsby-and-stripe/-/raw/master/screenshot.png)

Progect with Gatsby and CMS Strapi standard.
### Frontend Bienesraices

Start Develoment

```
cd frond-bienesraices
npm install
gatsby develop
```
Start Production

```
cd frond-bienesraices
gatsby build
```

### Backend Bienesraices

```
cd bienesraices
npm install
npm run develop
```

Url: http://localhost:5432/admin
GraphQL: http://localhost:5432/__graphql

## Preview
[Progect Demo](https://elastic-gates-e3fd08.netlify.app/)
